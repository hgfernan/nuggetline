# Architecture

Initially, there was a single `NuggetLine` class to handle `Nugget`s. And the `SourceNugget` and `TargetNugget` class would be derived from the `Nugget` class.

However, that is not a good idea, since a `NuggetLine` containing source coin -- therefore being used to **buy** assets --, will behave very differently when compared to a `NuggetLine` containing target coin, that will be used to **sell** assets.

Therefore, there will be only one `Nugget` class, and it will not be specialized in `SourceNugget` and `TargetNugget`. Instead, it is the `NuggetLine` that will be specialized as `SourceNuggetLine` and `TargetNuggetLine`.
