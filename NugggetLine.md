# Algorithm for the nugget line

## What is a *nugget*

In the context of this trading bot, trading is done in pairs of ***source*** *currency* and ***target*** *currency*. The **source** is usually a fiat currency, and the **target** is a cryptocoin that is intended to be traded.

A ***nugget*** is defined a fraction of value composed of a price, and an amount of target coin, and a flag indicating whether it actually contains target coin, or source coin. That is: if it contains **source** coin, the actual amount of source coin is calculated multiplying the price by the amount of target coin.

In Java terms, a nugget can be defined by a POJO (or *Plain Old Java Object*), as

```Java
class Nugget {
    bool target;
    double price;
    double amount;
}
```

Since it is devoted to trading, a nugget information block is used in the following way:

1. if it's a source currency nugget (the `target` member is set to `false`), it informs the price attained when selling a given amount of target currency.

    Of course the actual amount of source currency is calculated multiplicating the price attained and the amount of target currency sold;

2. if it's a target currency nugget (the `target` member is set to `true`), it informs the price attained when buying a given amount of target currency.

If target coin is being sold (using an *ask* item) in a value below the one in a source nugget, it is bought. That will transform the part bought in a target nugget.

If there's an offer to buy (using a *bid* item) in a value above the one in a target nugget, it is sold. That will transform the part sold in a source nugget.

The general idea is to sell a cryptocoin for more than what one paid for it, and to buy cryptocoin for less than what it was sold.

## What is a *nugget line*

Since for every sale or buying operation is usually not complete, there will be fragments of an initial ammount, that here are called nuggets. And since the market goes up and down, some of these nuggets can remain for some time.

For instance, a cryptocoin bought in a situation of higher prices will not be sold when the market is practicing lower prices. And it will remain there till the higher prices are back, when they will be sold for a higher price than they were bought.

A source nugget will create a target nugget when cryptocoin is bought. And reversely, a target nugget will create a source nugget when cryptocoin is sold.

A trading session will create two nugget lines: one for source currency, and the other for target currency.

In mathematical terms, a nugget line is an ordered sequence. And in computational terms, since it can have one new element inserted in its middle, between two existent nuggets, a nugget line can be easily implemented as a linked list.

## Minimal algorithm

The trading operation can be understood as a search in the one of the nugget lines.

For instance, if there's a cryptocoin sale, the source target is searched to find a price that's greater than the price offered, meaning that new cryptocoin will be bought by a price that's smaller than the price it was sold before.

If there is more than one source nugget whose price is smaller than the cryptocoin being sold, then the cheapest one is chosen. And if the amount in it is smaller than the cryptocoin offer, then other nuggets are added till either the offer or the nugget amounts are exhausted.

### Minimal algorithm for buying cryptocoin

Considering that `source_line` is an ordered sequence of source nuggets, that `offer` is a nugget of cryptocoin that has price `offer.price` and an amount `offer.amount`.

A new nugget named `target` is built that will have the amount of cryptocoin that is traded, provided that amount is available in the source nugget line.

#### 1st version of the buying algorithm -- Still without price conversions

```Java
source = source_line.head;
target.price = offer.price;
target.amount = 0;
while ((offer.amount > 0.0) && (offer.price <= source.price)) {
    offer.amount -= source.amount;
    target.amount += source.amount;

    source = source.next;
    if (source is null)
    {
        break;
    }
}
target_line.append(target);
```

#### 2nd version of the buying algorithm -- Doing price conversions

```Java
source = source_line.head;
target.price = offer.price;
target.amount = 0;
while ((offer.amount > 0.0) && (offer.price <= source.price)) {
    delta = source.amount * source.price;
    toBuy = delta / offer.price;
    toBuy = min(offer.amount, toBuy);
    offer.amount -= toBuy;
    target.amount += toBuy;

    source = source.next;
    if (source is null)
    {
        break;
    }
}
target_line.append(target);
```

#### 3rd version of the buying algorithm -- Eliminating exhausted nuggets

```Java
source = source_line.head;
target.price = offer.price;
target.amount = 0;
while ((offer.amount > 0.0) && (offer.price <= source.price)) {
    delta = source.amount * source.price;
    toBuy = delta / offer.price;
    toBuy = min(offer.amount, toBuy);
    offer.amount -= toBuy;
    target.amount += toBuy;

    source = source.next;
    if (source is null)
    {
        break;
    }
}
target_line.append(target);
source_line.vacuum();
```

#### 4th version of the buying algorithm -- Using only a fraction of the available nugget

```Java
source = source_line.head;
target.price = offer.price;
target.amount = 0;
while ((offer.amount > 0.0) && (offer.price <= source.price)) {
    delta = source.amount * source.price;
    toBuy = (delta / offer.price)*fraction;
    toBuy = min(offer.amount, toBuy);
    offer.amount -= toBuy;
    target.amount += toBuy;

    source = source.next;
    if (source is null)
    {
        break;
    }
}
target_line.append(target);
source_line.vacuum();
```

### Minimal algorithm for selling cryptocoin

## improvement towards realism in an exchange

## Improvement towards generalization

### Generalizing the minimal algorithm

### Generalizing the realist algoritm
